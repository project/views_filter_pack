<?php  // $Id$

/**
 * @file
 * The filters that handlers to cope with the effects of an
 * different form element. Most needed for checkboxes because
 * radios and select uses the same syntax in $element (see form.inc)
 */

/**
 * Handler for altering the query
 *
 * @param array $placeholders
 * @param integer $clauses_key
 * @param object $view
 * @param object $query
 * @param object $display
 * @param string $table
 * @param string $field
 * @param string $fieldtype
 */
function vfph_picture($placeholders, $clauses_key, &$view, &$query, $display, $table, $field, $fieldtype = NULL) {
  // Determine the exposed key for a field. This needed to determine the post
  // values.
  $exposed_key = _views_filter_pack_retrieve_exposed_key($view, $display, $table, $field);
  // Determine what values are posted.
  $formstate = _view_filter_pack_get_formstate($view, $display, $fieldtype, $table, $field, $exposed_key);

  // This is where we rewrite the query.
  switch ($fieldtype) {
    case 'checkboxes':
      if (is_array($formstate) && count($formstate)) {
        foreach ($formstate as $formstate_key => $formstate_value) {
          switch ($formstate_value) {
            case 0:
              $query_string[] = $table .".". $field ." = ''";
              break;
            case 1:
              $query_string[] = $table .".". $field ." <> ''";
              break;
          }
        }
        $query->where[0]['clauses'][$clauses_key] = "(". implode(" OR ", $query_string) .")";
      }
      else {
        // No filtering then no clause is needed.
        unset($query->where[0]['clauses'][$clauses_key]);
      }
      break;
    case 'radios':
      if (!is_array($formstate) && ($formstate <> 'All') && drupal_strlen($formstate)) {
        switch ($formstate) {
          case 0:
            $query->where[0]['clauses'][$clauses_key] = $table .".". $field ." = ''";
            break;
          case 1:
            $query->where[0]['clauses'][$clauses_key] = $table .".". $field ." <> ''";
            break;
        }
      }
      else {
        // No filtering then no clause is needed.
        unset($query->where[0]['clauses'][$clauses_key]);
      }
      break;
  }
  return;
}