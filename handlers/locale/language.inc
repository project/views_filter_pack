<?php  // $Id$

/**
 * @file
 * The filters that handlers to cope with the effects of an
 * different form element. Most needed for checkboxes because
 * radios and select uses the same syntax in $element (see form.inc)
 */

/**
 * Handler for altering the query
 *
 * @param array $placeholders
 * @param integer $clauses_key
 * @param object $view
 * @param object $query
 * @param object $display
 * @param string $table
 * @param string $field
 * @param string $fieldtype
 */
function vfph_language($placeholders, $clauses_key, &$view, &$query, $display, $table, $field, $fieldtype = NULL) {
  global $user, $language;

  // The view module defines some substitutions. Add the correct values to the
  // substitutions array.
  $substitutions['***CURRENT_LANGUAGE***'] = $user->language;
  $substitutions['***DEFAULT_LANGUAGE***'] = $language->language;
  $substitutions['***NO_LANGUAGE***'] = '';

  // Determine the exposed key for a field. This needed to determine the post
  // values.
  $exposed_key = _views_filter_pack_retrieve_exposed_key($view, $display, $table, $field);
  // Determine what values are posted.
  $formstate = _view_filter_pack_get_formstate($view, $display, $fieldtype, $table, $field, $exposed_key);
  // Build an array of placeholders.
  $build_placeholders = _views_filter_pack_build_placeholders($formstate);
  // prepare the arguments in the query object. Every placeholder should have an
  // argument.
  _views_filter_pack_alter_args($clauses_key, $query, $formstate);

  // Get the real value(s) instead of the substitutions
  if (is_array($formstate)) {
    foreach ($formstate as $formstate_key => $formstate_value) {
      if (is_string($formstate_value) && array_key_exists($formstate_key, $substitutions)) {
        $new_formstate[$substitutions[$formstate_key]] = $substitutions[$formstate_key];
      }
      else {
        $new_formstate[$formstate_key] = $formstate_key;
      }
      $formstate = $new_formstate;
    }
  }

  // String, so we are working with radio's.
  elseif (is_string($formstate) && array_key_exists($formstate, $substitutions)) {
    $formstate = $substitutions[$formstate];
  }

  // This is where we rewrite the query.
  switch ($fieldtype) {
    case 'checkboxes':
      if (is_array($formstate) && count($formstate)) {
        $query->where[0]['clauses'][$clauses_key] = $table .".". $field ." in(". $build_placeholders .")";
      }
      else {
        // No filtering then no clause is needed.
        unset($query->where[0]['clauses'][$clauses_key]);
      }
      break;
    case 'radios':
      if (!is_array($formstate) && ($formstate <> 'All') && drupal_strlen($formstate)) {
        $query->where[0]['clauses'][$clauses_key] = $table .".". $field ." = ". $build_placeholders ."";
      }
      else {
        // No filtering then no clause is needed.
        unset($query->where[0]['clauses'][$clauses_key]);
      }
      break;
  }

  return;
}