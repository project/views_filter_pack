<?php

/**
 * @file
 * override of an default views handler
 */

/**
 * Filter based on comment node status
 */
class vfp_handler_filter_node_comment extends vfp_handler_filter_in_operator {
  function get_value_options() {
    $this->value_options = array(
      COMMENT_NODE_DISABLED => t('Disabled'),
      COMMENT_NODE_READ_ONLY => t('Read only'),
      COMMENT_NODE_READ_WRITE => t('Read/Write'),
    );
  }
}
