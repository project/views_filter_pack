<?php // $Id$

/**
 * @file
 * override of an default views handler.
 */

/**
 * Filter by node type
 */
class vfp_handler_filter_node_type extends vfp_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Node type');
      $types = node_get_types();
      foreach ($types as $type => $info) {
        $options[$type] = t($info->name);
      }
      $this->value_options = $options;
    }
  }
}
