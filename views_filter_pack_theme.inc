<?php // $Id$

/**
 * @file
 * An override for theme functions.
 */

/**
 * Format a radio button.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: required, return_value, value, attributes, title, description
 * @return
 *   A themed HTML string representing the form item group.
 *
 * @ingroup themeable
 */
function _views_filter_pack_theme_radio($element) {
  // For some reason some <any> labels are rendered as expected while others
  // have html in the string like after a htmlentities function. Small check to
  // fix this behaviour.
  $any_label = variable_get('views_exposed_filter_any_label', 'old_any') == 'old_any' ? t('<Any>') : t('- Any -');

  $element['#title'] = decode_entities($element['#title']);
  if ($element['#title'] == $any_label) {
    $element['#title'] = htmlentities($element['#title']);
  }

  _form_set_class($element, array('form-radio'));
  $output = '<input type="radio" ';
  $output .= 'id="'. $element['#id'] .'" ';
  $output .= 'name="'. $element['#name'] .'" ';
  $output .= 'value="'. $element['#return_value'] .'" ';
  // BUG: for some reason the #post in the form is set with an empty array
  // resulting in a warning from the check plain function this should be fixed
  if (!is_array($element['#value'])) {
    $output .= (check_plain($element['#value']) == $element['#return_value']) ? ' checked="checked" ' : ' ';
  }
  $output .= drupal_attributes($element['#attributes']) .' />';
  if (!is_null($element['#title'])) {
    $output = '<label class="option" for="'. $element['#id'] .'">'. $output .' '. $element['#title'] .'</label>';
  }

  unset($element['#title']);
  return theme('form_element', $element, $output);
}