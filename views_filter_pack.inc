<?php // $Id$

/**
 * @file
 * All the small and greater function that help the module.
 */

/**
 * List of all the form types that could be altered when using an exposed view.
 * @return array
 */
function _views_filter_pack_alter_types() {
  return array('radios', 'checkboxes', 'select');
}

/**
 * Function to convert a select box to radios in a form.
 *
 * @param array $form
 * @param array $orig_element
 * @param string $table
 * @param string $field
 * @param object $display
 */
function _views_filter_pack_convert_select_to_radios(&$form, $orig_element, $table, $field, $display) {
  $form[$orig_element]['#type'] = 'radios';
  // Iterate through the values set the labels right.
  foreach ($form[$orig_element]['#options'] as $key => $value) {
    $form[$orig_element]['#options'][$key] = check_plain($value);
  }
  // Set a default value that exist to prevent the illegal choice validation
  // from form.inc (_form_validate($elements, &$form_state, $form_id = NULL))
  $form[$orig_element]['#value'] = array();
}

/**
 * Function to convert a select box to radios in a form.
 *
 * @param array $form
 * @param array $orig_element
 * @param string $table
 * @param string $field
 * @param object $display
 */
function _views_filter_pack_convert_select_to_checkboxes(&$form, $orig_element, $table, $field, $display) {
  // Retrieve the exposed key. Every field has an identifier to make sure that
  // fields are unique. So if there are two fields named 'status', views will
  // rename them to 'status' and 'status_1'. But also the user is able to rename
  // the identifier using views interface.
  $exposed_key = _views_filter_pack_retrieve_exposed_key($form['#parameters'][1]['view'], $display, $table, $field);

  // Get the formstate for the current exposed field. The field is identified by
  // the exposed key.
  $formstate = _view_filter_pack_get_formstate($form['#parameters'][1]['view'], $display, 'checkboxes', $table, $field, $exposed_key);

  $form[$orig_element]['#type'] = 'checkboxes';
  // Iterate through the values set the label right. The label all can't exist.
  unset($form[$orig_element]['#options']['All']);
  $elements = $form[$orig_element]['#options'];

  // Below we are about to alter the keys. First we check if this is needed.
  // Retrieve all fields from hook_register_fields.
  $field_information = views_filter_pack_register_fields();

  // Check for the current field is prefixing the keys is need.
  if ($field_information[$table .'.'. $field]['alter_keys']) {
    unset($form[$orig_element]['#options']);
    // Rebuild the options because else there is the following situation: you
    // have posted a checkbox with the value 0, the rendering of the form will
    // see 0 as a reason to uncheck it. That is why we add a prefix k_ this way
    // the key will be an string and the behavior is fixed.
    foreach ($elements as $key => $value) {
      $form[$orig_element]['#options']["k_". $key] = $value;
    }
  }

  // Set a default value that exist to prevent the illegal choice validation
  // from form.inc (_form_validate($elements, &$form_state, $form_id = NULL))
  $form[$orig_element]['#value'] = $value;
}

/**
 * Count the placeholders that are used per clause.
 *
 * @param array $clauses
 * @return array $placeholders per clause
 */
function _views_filter_pack_get_placeholders($clauses) {
  $placeholders = array();
  if (is_array($clauses)) {
    foreach ($clauses as $key => $value) {
      $placeholders[$key] = substr_count($value, '%s');
      $placeholders[$key] += substr_count($value, '%d');
    }
  }
  return $placeholders;
}

/**
 * Get the clause matching with the field. This function returns the key of a
 * clause.
 *
 * @param string $field
 * @param array $clauses
 * @param string $alias
 * @return integer
 */
function _views_filter_pack_get_clauses_key($field, $clauses, $alias = NULL) {
  // Use the alias to find clauses key aliasses are often used if a field has
  // more than one filter.
  if (isset($alias)) {
    $field = $alias;
  }

  if (is_array($clauses)) {
    foreach ($clauses as $key => $value) {
      if (strstr($value, $field)) {
        return $key;
      }
    }
  }
}

/**
 * The exposed form has an certain state. Values are submitted or not. If no
 * values are submitted than you probaly arrive on the display for the first
 * time. If so then use the default values from the query. This function returns
 * a array describing the formstate.
 *
 * @param object $view
 * @param object $display
 * @param string $fieldtype
 * @param string $table
 * @param string $field
 * @param integer $exposed_key
 * @return array $formstate
 */
function _view_filter_pack_get_formstate($view, $display, $fieldtype, $table, $field, $exposed_key) {
  // Check if formstate is set else take default from exposed input in view.
  if (!is_null($view->exposed_input[$exposed_key])) {
    $formstate = $view->exposed_input[$exposed_key];
  }
  else {
    // If results are submitted but current field filter has no results.
    if (count($view->exposed_input)) {
      $formstate = array();
    }
    // Else you have come to the display the first time so use default
    else {
      $formstate = $view->display[$display]->display_options['filters'][$exposed_key]['value'];
    }
  }

  // It is possible that the keys have been altered. They need to be restored.
  $field_information = views_filter_pack_register_fields();
  if ($field_information[$table .'.'. $field]['alter_keys'] && ($fieldtype == 'checkboxes')) {
      $formstate = _views_filter_pack_strip_prefix($formstate);
  }

  return $formstate;
}

/**
 * A simple function to strip a prefix from an array with values.
 * @param array $values
 * @return array $new_values
 */
function _views_filter_pack_strip_prefix($values) {
  if (is_array($values) && count($values)) {
    $new_values = array();
    foreach ($values as $key => $value) {
      // Check first if the substring matches with the value we want to strip.
      if (drupal_substr($key, 0, 2) == 'k_') {
        $new_values[drupal_substr($key, 2)] = drupal_substr($value, 2);
      }
    }
  }
  return $new_values;
}

/**
 * Funtion to remove a where clause if it is not needed and if all where clauses
 * are gone it alters the array to prevent problems in query.inc of views.
 * @param object $query
 * @param integer $clauses_key
 */
function _views_filter_pack_remove_where_clause(&$query, $clauses_key) {
  unset($query->where[0]['clauses'][$clauses_key]);
  if (count($query->where[0]['clauses']) == 0) {
    unset($query->where[0]);
  }
}

/**
 * Function where we keep track of all the fields and their behaviour because
 * some fields use an other handler and so they act differently on the form
 * alter and query alters we do. And it calls module invoke so other modules can
 * edit, alter and add fields to it.
 *
 * @return array $fields
 */
function views_filter_pack_register_fields() {
  $fields = array();
  $fields = module_invoke_all('register_filter_fields');

  return $fields;
}

/**
 * This function deals with duplicate field names. example 'status'. The first
 * can be found as 'status' but the second is found as 'status_1'. We'll walk
 * through the $view object to recover the name that might be used. This
 * function helps determing the form state.
 *
 * @param object $view
 * @param object $display
 * @param string $table
 * @param string $field
 * @return integer $exposed_key
 *
 */
function _views_filter_pack_retrieve_exposed_key($view, $display, $table, $field) {
  $array = $view->display[$display]->display_options['filters'];

  // If no array has been found then try to find the default display.
  if (!is_array($array)) {
    $array = $view->display['default']->display_options['filters'];
  }

  if (is_array($array)) {
    foreach ($array as $key => $value) {
      if (($value['table'] == $table) && ($value['field'] == $field)) {
        return $value['expose']['identifier'];
      }
    }
  }
}

/**
 * Prepare the amount of placeholders that need to be used. These placeholders
 * will return to the handler function and explode into a string of placeholders
 * in the clause. E.g. "node.status in('%s', '%s')".
 *
 * @param array $formstate
 * @return array or string
 */
function _views_filter_pack_build_placeholders($formstate) {
  if (is_array($formstate)) {
    foreach ($formstate as $key => $value) {
      $string[] = "'%s'";
    }
    if (count($string)) {
      return implode(',', $string);
    }
  }
  else {
    return '%s';
  }
}

/**
 * This function counts the amount of args before of the current clause. This is
 * a function that helps inserting arguments at the right point in the array of
 * arguments.
 *
 * @param integer $clauses_key
 * @param object $query
 * @param array $formstate
 */
function _views_filter_pack_alter_args($clauses_key, &$query, $formstate) {
  if (is_array($query->where[0]['clauses'])) {
    $parent_clauses = array_slice($query->where[0]['clauses'], 0, $clauses_key);
  }
  $parent_placeholders = _views_filter_pack_get_placeholders($parent_clauses);
  // Determine insert point by calculating how much placeholder the parents
  // have.
  if (is_array($parent_placeholders)) {
    $i = 0;
    foreach ($parent_placeholders as $key => $value) {
      $i += $value;
    }
  }

  // Add the values to the args array of the view object. The formstate can be
  // an array of values of just a string.
  if (is_array($formstate) && isset($i)) {
    foreach ($formstate as $key => $value) {
      array_splice($query->where[0]['args'], $i, 0, $value);
    }
  }
  elseif (!is_array($formstate) && drupal_strlen($formstate) && ($formstate <> 'All')) {
    array_splice($query->where[0]['args'], $i, 0, $formstate);
  }
}

/**
 * Function to build an array of arguments. This function makes sure that if
 * a field is exposed and handled by Views Filter pack, that at the right point
 * in the arguments array also the right amount of values is added. The result
 * is an array of arguments in the right order for the clauses.
 *
 * @param object $view
 * @param object $query
 * @param object $display_filters
 */
function _views_filter_pack_clean_args(&$view, &$query, &$display_filters) {
  // Get all fields and loop every field so see it they will be altered by this
  // module, if so clean up the arguments.
  $fields_information = views_filter_pack_register_fields();

  // We will check the following information per clause:
  // 1) Calculate placeholders per clause.
  // 2) Can this field be altered by this module?
  // 3) Is this field exposed by views?
  // We add this information to the $query object.

  // Step 1: Calculate placeholders per clause.
  $query->where[0]['placeholders'] = _views_filter_pack_get_placeholders($query->where[0]['clauses']);

  // Step 2: Find all fields that could be handled by this module.
  if (is_array($fields_information)) {
    foreach ($fields_information as $field_key => $field_value) {
      $alias = NULL;
      $clauses_key = NULL;
      if (isset($field_value['alias'])) {
        $alias = $field_value['alias'];
      }
      $clauses_key = _views_filter_pack_get_clauses_key($field_key, $query->where[0]['clauses'], $alias);
      if (is_int($clauses_key)) {
        $query->where[0]['is_handled_by_vfp'][$clauses_key] = TRUE;
      }
    }
  }

  // Step 3: Check if the field is exposed, the filters are shown in the order
  // of the clauses
  foreach ($display_filters as $filter_key => $filter_value) {
    if ($filter_value['exposed']) {
      $query->where[0]['is_exposed'][] = 1;
    }
    else {
      $query->where[0]['is_exposed'][] = 0;
    }
  }

  // Now we have all the information we can create an array with all the keys of
  // the arguments that have to be removed.
  $mirror_array = array();
  foreach ($query->where[0]['placeholders'] as $mirror_key => $mirror_value) {
    $offset_value += $mirror_value;
    $pad_value = 0;
    if (($query->where[0]['is_handled_by_vfp'][$mirror_key]) && ($query->where[0]['is_exposed'][$mirror_key])) {
      $pad_value = 1;
    }
    $mirror_array = array_pad($mirror_array  , $offset_value  , $pad_value);
  }

  // Now we have an array of all the args that have to remove. Lets remove them.
  // The result is an array of arguments in the right order for the clauses.
  foreach ($mirror_array as $remove_key => $remove_value) {
    if ($remove_value) {
      unset($query->where[0]['args'][$remove_key]);
    }
  }
}